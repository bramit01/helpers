package nl.bramit.password;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import lombok.Getter;

public class PasswordMain {

	//private static Logger LOGGER = LoggerFactory.getLogger(PasswordMain.class.getName());
	//private static String _VERSION = StaticUtils.getVersion(PasswordMain.class.getResourceAsStream("/password.properties"));

	@Parameter(names = { "-e", "--encrypt" }, description = "Encrypt textstring")
	@Getter private static String passwordUnencrypted = "";
	@Parameter(names = { "-d", "--decrypt" }, description = "Decrypt textstring")
	@Getter private static String passwordEncrypted = "";
	@Parameter(names = { "-h", "--help" }, help = true, description = "Show help")
	@Getter private Boolean isHelp = false;

	public void run(){
		Password password = new Password (getPasswordUnencrypted(), getPasswordEncrypted());
		try{
			System.out.println(password.toString());
		}catch (EncryptionOperationNotPossibleException e){
			System.out.println("'" + getPasswordEncrypted() + "'" + " is not a valid encrypted password");
		}
	}

	public static void main(String[] args) {
		PasswordMain pwm = new PasswordMain();
		JCommander jCommander = new JCommander(pwm, args);
		if ((args.length == 0)) {
			jCommander.usage();
			return;
		}
		if (pwm.isHelp) {
			jCommander.usage();
			return;
		}
		pwm.run();
	}
}
