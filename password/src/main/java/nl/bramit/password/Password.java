package nl.bramit.password;

import lombok.Getter;
import lombok.Setter;
import nl.bramit.helpers.EncryptUtils;

public class Password {
	@Getter @Setter private String passwordUnencrypted;
	@Getter @Setter private String passwordEncrypted;
	
	public Password(String passwordUnencrypted, String passwordEncrypted) {
		setPasswordUnencrypted(passwordUnencrypted);
		setPasswordEncrypted(passwordEncrypted);
	}
	
	@Override
	public String toString(){
		if (getPasswordUnencrypted().equals("")){
			if (getPasswordEncrypted().equals("")){
				//nothing to do
				return "";
			}else{
				//Encrypted password needs to be decrypted
				return EncryptUtils.decrypt(getPasswordEncrypted());
			}
		}else{
			//Decrypted password needs to be encrypted
			return EncryptUtils.encrypt(getPasswordUnencrypted());				
		}
	}

}
