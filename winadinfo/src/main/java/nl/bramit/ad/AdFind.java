package nl.bramit.ad;

import java.io.File;
import java.util.Enumeration;
import java.util.Properties;

import javax.management.RuntimeErrorException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import lombok.Getter;
import lombok.Setter;
import nl.bramit.helpers.ldap.ActiveDirectory;

public class AdFind {
	@Parameter(names = { "-an", "--adreadername" }, description = "Windows AD Reader username")
	@Getter
	private String adReaderName = System.getProperty("user.name");
	@Parameter(names = { "-ap",
			"--adreaderpw" }, description = "Windows AD Reader password, after first entry, password will be stored in a properties file in your home location")
	@Getter
	@Setter
	private String adReaderPassword = "";
	@Parameter(names = { "-ad", "--adreaderdom" }, description = "Windows AD Reader domain")
	@Getter
	private String adReaderDomain = System.getenv("USERDNSDOMAIN");
	@Parameter(names = { "-ug",
			"--usergroup" }, description = "USERGROUP related query, otherwise a USER related query")
	@Getter
	private Boolean isUserGroup = false;
	@Parameter(names = { "-s", "--search" }, required = true, description = "what to search for")
	@Getter
	private String searchValue;
	@Parameter(names = { "-sb",
			"--searchby" }, description = "context of the searchvalue, values possible: name, sid, email, cn, distinguishedName")
	@Getter
	@Setter
	private String searchKind = "email";
	@Parameter(names = { "-sd", "--searchdomain" }, description = "Windows AD domain to search in")
	@Getter
	private String searchDomain = "eu.rabonet.com";
	@Parameter(names = { "-a", "--attribute" }, description = "Attribute that is printed. e.g. memberOf")
	@Getter
	private String attribute = "ALL";
	@Parameter(names = { "-d", "--debug" }, hidden = true, description = "Debug mode")
	@Getter
	private Boolean isDebug = false;
	@Parameter(names = { "-h", "--help" }, help = true, description = "Show help")
	@Getter
	private Boolean isHelp = false;
	@Parameter(names = { "-os", "--outputseparator" }, description = "Output column separator, be aware that AD attributes can contain comma's, default = TAB")
	@Getter
	private String outputSeparator = "\t";

	private ActiveDirectory activeDirectory = null;
	private String propertyFileName = System.getProperty("user.home") + "\\grs.adfind.properties";
	private String propertyKey = "ad.user.password";
	private String isPropertyKeyEncrypted = "is.ad.user.password";

	void printProperties() {
		Properties properties = System.getProperties();
		Enumeration<?> keys = properties.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = (String) properties.get(key);
			System.out.println(key + outputSeparator + value);
			System.out.println("");
		}
	}

	@SuppressWarnings("rawtypes")
	void printAttrs(Attributes attrs) {
		if (attrs == null) {
			System.out.println("ERROR" + outputSeparator + "No attributes to print");
		} else {
			/* Print each attribute */
			try {
				for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
					Attribute attr = (Attribute) ae.next();
					if (attribute.equals("ALL")) {
						for (NamingEnumeration e = attr.getAll(); e.hasMore();) {
							System.out.println(attr.getID() + outputSeparator + e.next());
						}
					} else {
						if (attribute.equals(attr.getID())) {

							for (NamingEnumeration e = attr.getAll(); e.hasMore();) {
								System.out.println(attr.getID() + outputSeparator + e.next());
							}
						}
					}
				}

			} catch (NamingException e) {
				System.out.println("ERROR" + outputSeparator + "Error in printing attributes");
				if (isDebug) {
					e.printStackTrace();
				}
			}
		}

	}

	private AdFind() {
	}

	private void run() throws NamingException, ConfigurationException {

	
		if (getAdReaderPassword().equals("")) {
			//get password from application properties file in home folder
			setAdReaderPassword(getPassword());
		} else {
			if (getAdReaderName().equals(System.getProperty("user.name")))
				// save encrypted in a file if using own user.name
				savePasswordInFile();
		}
		// TODO: change username to name in ActiveDirectory class
		if (getSearchKind().equals("name")) {
			setSearchKind("username");
		}

		System.out.println("Attribute" + outputSeparator + "Value");
		System.out.println("SEARCH: adreadername" + outputSeparator + adReaderName);
		if (isDebug)
			System.out.println("SEARCH: adreaderpw" + outputSeparator + adReaderPassword);
		System.out.println("SEARCH: adreaderdom" + outputSeparator + adReaderDomain);
		System.out.println("SEARCH: usergroup" + outputSeparator + isUserGroup);
		System.out.println("SEARCH: searchvalue" + outputSeparator + searchValue);
		System.out.println("SEARCH: searchkind" + outputSeparator + searchKind);
		System.out.println("SEARCH: searchdomain" + outputSeparator + searchDomain);
		System.out.println("SEARCH: debug" + outputSeparator + isDebug);

		activeDirectory = new ActiveDirectory(adReaderName, adReaderPassword, adReaderDomain);
		if (null == activeDirectory) {
			System.out.println("ERROR" + outputSeparator + "Error in initialising Active Directory object");
		} else {
			// test
			// activeDirectory.setDirContext((DirContext)
			// activeDirectory.getDirContext().addToEnvironment(Context.SECURITY_AUTHENTICATION,
			// "none"));
			NamingEnumeration<SearchResult> result;

			if (!isUserGroup) {
				result = activeDirectory.searchUser(searchValue, searchKind, searchDomain);
			} else {
				result = activeDirectory.searchGroup(searchValue, searchKind, searchDomain);
			}

			/*
			 * if ((result != null) && (result.hasMore())){ int count = 0;
			 * SearchResult firstHit = result.nextElement(); if
			 * (result.hasMoreElements()){
			 * //printAttrs(firstHit.getAttributes());
			 * 
			 * count++; System.out.println(count + "\t" + firstHit.getName());
			 * for (SearchResult s = result.next(); result.hasMoreElements();) {
			 * count++; System.out.println(count + "\t" + s.getName());
			 * //printAttrs(s.getAttributes()); } }else{ Attributes attrs =
			 * firstHit.getAttributes(); printAttrs(attrs); } }
			 * 
			 * if (result.hasMore()){ SearchResult rs=
			 * (SearchResult)result.next();
			 * 
			 * }else{ System.out.println("ERROR" + "\t" +
			 * "Nothing found for this search"); }
			 */

			int count = 0;
			SearchResult searchResult = null;
			while (result.hasMoreElements()) {
				count++;
				searchResult = (SearchResult) result.next();
				System.out.println(count + outputSeparator + searchResult.getName());

			}

			if (count == 1) {
				printAttrs(searchResult.getAttributes());
			}

			activeDirectory.closeLdapConnection();
		}

	}

	private void savePasswordInFile() throws ConfigurationException {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(AdFind.class.getName());
		String encryptedPassword = encryptor.encrypt(getAdReaderPassword());
		// File f = new File(propertyFileName);
		// if (f.exists()){
		PropertiesConfiguration config = new PropertiesConfiguration(new File(propertyFileName));
		config.setProperty(propertyKey, encryptedPassword);
		config.setProperty(isPropertyKeyEncrypted, "true");
		config.save();
	}

	private String getPassword() throws ConfigurationException {
		PropertiesConfiguration config = null;
		File f = new File(propertyFileName);
		if (f.exists()) {
			config = new PropertiesConfiguration(propertyFileName);
			String isEncrypted = (String) config.getProperty(isPropertyKeyEncrypted);
			if (isEncrypted.equals("true")) {
				String encryptedPassword = (String) config.getProperty(propertyKey);
				StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
				encryptor.setPassword(AdFind.class.getName());
				return encryptor.decrypt(encryptedPassword);
			} else {
				return (String) config.getProperty(propertyKey);
			}

		} else {
			throw new RuntimeErrorException(
					new Error("Cannot get saved password, please use option --adreaderpw for the AD password if you run this application for the first time"));
		}
	}

	public static void main(String[] args) throws NamingException, ConfigurationException {
		AdFind adFind = new AdFind();
		JCommander jCommander = new JCommander(adFind, args);
		if ((args.length == 0)) {
			jCommander.usage();
			return;
		}
		if (adFind.isHelp) {
			jCommander.usage();
			return;
		}
		adFind.run();
	}
}
