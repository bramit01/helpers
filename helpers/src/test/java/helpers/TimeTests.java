package helpers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import nl.bramit.helpers.TimeUtils;


//http://bloodredsun.com/2014/06/03/checking-logback-based-logging-in-unit-tests/
//@RunWith(MockitoJUnitRunner.class)
public class TimeTests {

	@SuppressWarnings("rawtypes")
	@Mock
	private Appender mockAppender;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
	
	TimeUtils timeUtils;
	final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
	String standardOutput = "";
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);	
		final Logger LOGGER= (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		LOGGER.addAppender(mockAppender);
		//redirect standard output to a string
		//System.setOut(new PrintStream(myOut));

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		TimeUtils.useFixedClockAt(LocalDateTime.parse("2016-10-28 15:14:13", formatter)); //on a Friday
		Set<LocalDate> holidays = new HashSet<>();
		//holidays.add(LocalDate.parse("2016-12-31"));
		holidays.add(TimeUtils.now().toLocalDate());
		holidays.add(TimeUtils.now().plusDays(3).toLocalDate()); //add the next Monday to the hiolidaylist

		TimeUtils.addHolidayCalendar("NL", holidays);

	}
	@SuppressWarnings("unchecked")
	@Test
	public void wrongFormat() {
		String formatted = TimeUtils.convertDateTimeVariable("CURRENT|DAY|YYYYMMDD");
		assertThat(formatted,is(""));
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("Format contains an invalid value")));
		
	}


	@SuppressWarnings("unchecked")
	@Test
	public void currentWeekShouldReturnError() {
		String formatted = TimeUtils.convertDateTimeVariable("CURRENT|WEEK|YYYYMMdd");
		assertThat(formatted,is(""));
		
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("is not valid for")));

	}

	@SuppressWarnings("unchecked")
	@Test
	public void nextDayWithoutStepOK() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|DAY|YYYYMMdd");
		assertThat(formatted,is("20161029"));
	}
	@SuppressWarnings("unchecked")
	@Test
	public void nextDayStepNotNumeric() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|TWEE|DAY|YYYYMMdd");
		assertThat(formatted,is(""));
		
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("Step contains an invalid value")));

	}
	@SuppressWarnings("unchecked")
	@Test
	public void periodIsWrong() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|2|WHATISTHIS|YYYYMMdd");
		assertThat(formatted,is(""));
		
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("Period contains an invalid value")));

	}
	@Test
	public void nextDayInWeekend() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|1|DAY|YYYYMMdd");
		assertThat(formatted,is("20161029"));
	}
	@Test
	public void nextBusinessDayInWeekend() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|1|BUSINESSDAYNL|YYYYMMdd");
		assertThat(formatted,is("20161101")); //20161031 is a holiday!
	}
	@SuppressWarnings("unchecked")
	@Test
	public void businessDayWithTimeFormat() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|2|BUSINESSDAYNL|YYYYMMdd HH:ss:mm");
		assertThat(formatted,is(""));
		
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("Error in formatting")));

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void formatContainsUnderscoreWithStep() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|2|BUSINESSDAYNL|YYYYMMdd_HH:ss:mm");
		assertThat(formatted,is(""));
		
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("Error in formatting")));

	}
	@SuppressWarnings("unchecked")
	@Test
	public void formatContainsUnderscoreWithoutStep() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|BUSINESSDAYNL|YYYYMMdd_HH:ss:mm");
		assertThat(formatted,is(""));
		
		verify(mockAppender).doAppend(captorLoggingEvent.capture());
		final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
		assertThat(loggingEvent.getLevel(), is(Level.ERROR));
		assertThat(loggingEvent.getFormattedMessage(), is(containsString("Error in formatting")));

	}
	
	@Test
	public void nextWeekOK() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|1|WEEK|YYYYMMdd");
		assertThat(formatted,is("20161104")); 
	}
	@Test
	public void nextMonthOK() {
		String formatted = TimeUtils.convertDateTimeVariable("NEXT|3|MONTH|YYYYMMdd");
		assertThat(formatted,is("20170128")); 
	}
	
	@Test
	public void dateTimeOnly() {
		String formatted = TimeUtils.convertDateTimeVariable("dd MMM yyyy HH:mm:ss");
		assertThat(formatted,is("28 Oct 2016 15:14:13")); 
	}
	
	@Test
	public void dateTimeOnly2() {
		String formatted = TimeUtils.convertDateTimeVariable("ddMMyyyy");
		assertThat(formatted,is("28102016")); 
	}


	@SuppressWarnings("unchecked")
	@After
	public void teardown(){
		//standardOutput = myOut.toString();
		final Logger LOGGER = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		LOGGER.detachAppender(mockAppender);
	}
}
