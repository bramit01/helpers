package nl.bramit.helpers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import nl.bramit.helpers.exceptions.IniManagerException;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile.Section;

/**
 * Inimanager for managing windows ini files
 * @version 1.0.0
 * @author johandewaardt
 * @throws IniManagerException
 * uses external ini library: ini4j-0.5.2.jar (http://ini4j.sourceforge.net/index.html)
 * http://ini4j.sourceforge.net/tutorial/IniTutorial.java.html
 */
 
public class IniManager {
	//public static final String FILENAME = "filewatch.ini";
	private Ini oIni; 
	private String m_IniFile;
	
	/**
	 * Constructor, loads the ini file 
	 * @param psFile
	 * @throws IniManagerException
	 */
	
	public IniManager() throws IniManagerException{
		this("Default.ini");
	}
	public IniManager(String psFile) throws IniManagerException{
			
			oIni = new Ini();
			try {
				oIni.load(new FileReader(psFile));
			} catch (InvalidFileFormatException e) {
				throw new IniManagerException("Cannot load ini file: [" + psFile + "], InvalidFileFormatException: [" + e.getMessage() + "]", e);
			} catch (FileNotFoundException e) {
				try {
					//inifile is not available, create an example
					oIni.put("Example Section", "Key", "some value");
					oIni.store(new FileWriter(psFile));
					throw new IniManagerException("Ini file not available, an example has been created: [" + psFile + "]", e);
				} catch (IOException e1) {
					throw new IniManagerException("Cannot create new ini file: [" + psFile + "], IOException: [" + e.getMessage() + "]", e);
				}
			} catch (IOException e) {
				throw new IniManagerException("Cannot load ini file: [" + psFile + "], IOException: " + e.getMessage() + "]", e);
			} finally {
				m_IniFile = psFile;
			}
	}
	public Map<String, String> getKeyValues(String psSection){
		Map<String, String> mpSectionValues = oIni.get(psSection);

		return mpSectionValues;
	}
	/**
	 * Function to return the value of a specified key in the named section
	 * @param psSection
	 * @param psKeyName
	 * @param psDefault
	 * @return			>Value of the Key in the Section, of psDefault if the Value is not found
	 */
	public String getKeyValue(String psSection, String psKeyName, String psDefault){
		Section oSection = oIni.get(psSection);
		String sResult = oSection.get(psKeyName);
		
		return sResult;
		        
	}
	/**
	 * Method to store KeyValue pair in the defined section of the file: m_IniFile
	 * @param psSection				>Section of the ini file
	 * @param psKeyName				>Keyname
	 * @param psValue				>Value to be stored
	 * @throws IniManagerException
	 */
	public void setKeyValue(String psSection, String psKeyName, String psValue) throws IniManagerException{
		oIni.put(psSection, psKeyName, psValue);
		try {
			oIni.store(new FileWriter(m_IniFile));
		} catch (IOException e) {
			throw new IniManagerException(String.format("IOException occured, Section: [{0}], Keyname: [{1}], Value: [{2}]", psSection, psKeyName, psValue), e);		
			}
	}
}

	
	
	


