
package nl.bramit.helpers.ldap; 

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

public class ActiveDirectory {
	// Logger
	private static final Logger LOGGER = LoggerFactory.getLogger(ActiveDirectory.class.getName());

    //required private variables   
    private Properties properties;
    @Getter @Setter private DirContext dirContext;
    private SearchControls searchCtls;
  //private String[] returnAttributes = { "objectSID", "sAMAccountName", "givenName", "cn", "mail" };
//	private String[] returnAttributes = { "objectSID"
//			, "sAMAccountName"
//			, "givenName"
//			, "cn"
//			, "mail"
//			, "distinguishedName"
//			, "displayName"
//			, "c"
//			, "co"
//			, "company"
//			, "department"
//			, "employeeID"
//			, "employeeNumber"
//			, "extensionAttribute2"
//			, "initials"
//			, "l"
//			, "physicalDeliveryOfficeName"
//			, "postalCode"
//			, "sn"
//			, "streetAddress"
//			, "telephoneNumber"
//			, "whenCreated"};
    private String domainBase;
    //private String baseFilter = "(&((&(objectCategory=Person)(objectClass=User)))";
    private String baseFilter = "(&((&(objectCategory=Person)(objectClass=*)))";
    private String baseFilterGroups = "(&((&(objectCategory=Group)(objectClass=*)))";
    @Getter private boolean isWindows;

    /**
     * constructor with parameter for initializing a LDAP context
     * 
     * @param username a {@link java.lang.String} object - username to establish a LDAP connection
     * @param password a {@link java.lang.String} object - password to establish a LDAP connection
     * @param domainController a {@link java.lang.String} object - domain controller name for LDAP connection
     */
    public ActiveDirectory(String username, String password, String domainController) {
    	//check if we are on Windows
    	if (SystemUtils.IS_OS_WINDOWS) {
	    	/*
	    	 * 525 - User not found
	    	 * 52e - Invalid Credentials
	    	 * 530 - Not permitted to logon at this time
	    	 * 531 - Not permitted to logon at this workstation
	    	 * 532 - Password expired
	    	 * 533 - Account disabled
	    	 * 701 - Account expired
	    	 * 773 - User must reset password
	    	 * 775 - User account locked
	    	 */
	    	properties = new Properties();        
	
	        properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	        properties.put(Context.PROVIDER_URL, "LDAP://" + domainController);
	        properties.put(Context.SECURITY_PRINCIPAL, username + "@" + domainController);
	        properties.put(Context.SECURITY_CREDENTIALS, password);
	        properties.put(Context.REFERRAL, "follow");
	        properties.put("java.naming.ldap.attributes.binary", "objectSID");
	        
	        //initializing active directory LDAP connection
	        try {
				dirContext = new InitialDirContext(properties);
		        //LOGGER.debug("Created initial context: ");
		        //properties.list(System.out);
		        //default domain base for search
		        domainBase = getDomainBase(domainController);
		        //LOGGER.debug("Domain controller set to {}", domainController);
		        //initializing search controls
		        searchCtls = new SearchControls();
		        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		        searchCtls.setCountLimit(new Long (1000));
		        //searchCtls.setReturningAttributes(returnAttributes);
		        //LOGGER.debug("Searchscope set to SUBTREE_SCOPE");
	        } catch (NamingException e) {
				LOGGER.error("Error in creating InitialDirContext", e);
	        	LOGGER.error("domainController = [" + domainController + "]; username = [" + username + "]; password = [****]");
			}
	        this.isWindows = true;
    	}else{
    		this.isWindows = false;
    	}
    }
    
 
    public NamingEnumeration<SearchResult> searchUser(String searchValue, String searchBy, String searchBase) throws NamingException {
    	if (!isWindows()){
    		LOGGER.warn("Operating System is not Windows, cannot search AD");
    		return null;
    		}
    	
    	if (null == this.dirContext){
    		LOGGER.warn("dirContext is null, problem in initializing ActiveDirectory object?");
    		return null;
    		};
    	
    	String filter = getFilter(baseFilter, searchValue, searchBy);    	
    	String base = (null == searchBase) ? domainBase : getDomainBase(searchBase);
    	
		return this.dirContext.search(base, filter, this.searchCtls);
    }

    public String searchGroupBySID(String searchValue, String searchBase) throws NamingException {
    	if (!isWindows()){
    		LOGGER.warn("Operating System is not Windows, cannot search AD");
    		return null;
    		}
    	
    	if (null == this.dirContext){
    		LOGGER.warn("dirContext is null, problem in initializing ActiveDirectory object?");
    		return null;
    		};
        
    	String filter = baseFilterGroups; 
    	filter += "(objectSid=" + searchValue + "))";  	
    	String base = (null == searchBase) ? domainBase : getDomainBase(searchBase); // for eg.: "DC=myjeeva,DC=com";
    	String groupName = "";
    	
    	NamingEnumeration<SearchResult> result = this.dirContext.search(base, filter, this.searchCtls);
    	if(result.hasMore()) {
 			SearchResult rs= (SearchResult)result.next();
 			//Attributes attrs = rs.getAttributes();
 			groupName = getNameFromAttributes(rs.getAttributes(), "samaccountname");
 		} else  {
 			groupName = "";
 		}
    	return groupName;
    }

    public NamingEnumeration<SearchResult> searchGroup(String searchValue, String searchBy, String searchBase) throws NamingException {
    	if (!isWindows()){
    		LOGGER.warn("Operating System is not Windows, cannot search AD");
    		return null;
    		}
    	
    	if (null == this.dirContext){
    		LOGGER.warn("dirContext is null, problem in initializing ActiveDirectory object?");
    		return null;
    		};
        
    	String filter = getFilter(baseFilterGroups, searchValue, searchBy);  	
    	String base = (null == searchBase) ? domainBase : getDomainBase(searchBase); // for eg.: "DC=myjeeva,DC=com";
    	
    	return this.dirContext.search(base, filter, this.searchCtls);

    }

    
    /**
     * closes the LDAP connection with Domain controller
     */
    public void closeLdapConnection(){
        try {
            if(dirContext != null)
                dirContext.close();
        }
        catch (NamingException e) {
        	LOGGER.error("Could not close LDAP connection", e);            
        }
    }
    

    public static String getNameFromAttributes(Attributes attrs, String propertyString){
    	if (attrs.get(propertyString) == null){
    		return "";
    	}else{
    		return  attrs.get(propertyString).toString().substring(attrs.get(propertyString).toString().indexOf(":")+2);
    	}
    }
    private String getFilter(String baseFilter, String searchValue, String searchBy) {
    	String filter = baseFilter;
    	if(searchBy.equals("email")) {
    		filter += "(mail=" + searchValue + "))";
    	} else if(searchBy.equals("username")) {
    		filter += "(samaccountname=" + searchValue + "))";
    	} else if(searchBy.equals("sid")){
    		filter += "(objectSid=" + searchValue + "))";
       	} else if(searchBy.equals("cn")){
    		filter += "(cn=" + searchValue + "))";
    	} else if(searchBy.equals("distinguishedName")){
    		filter += "(distinguishedName=" + searchValue + "))";
    	}

		return filter;
	}
    
  
	private static String getDomainBase(String base) {
		char[] namePair = base.toUpperCase().toCharArray();
		String dn = "DC=";
		for (int i = 0; i < namePair.length; i++) {
			if (namePair[i] == '.') {
				dn += ",DC=" + namePair[++i];
			} else {
				dn += namePair[i];
			}
		}
		return dn;
	}
}
