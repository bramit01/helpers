package nl.bramit.helpers;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.jasypt.util.text.BasicTextEncryptor;

//If error in the two libs beneath, check Eclipse properties: 
// -JAva Compiler/Errors and Warnings/Depreciated and Restricted Api/Set to Warning instead of Error
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Encrypt / Decrypt Utilities
 * @version 1.0.0
 * @author johandewaardt
 *
 */
@SuppressWarnings("restriction")
public class EncryptUtils {
	   public static final String DEFAULT_ENCODING="UTF-8"; 
	   static BASE64Encoder enc=new BASE64Encoder();
	   static BASE64Decoder dec=new BASE64Decoder();

	   private static final String PW = "Iguanod0n";
	   private static BasicTextEncryptor textEncryptor = new BasicTextEncryptor();

	   public static String encrypt (String text){
		   textEncryptor.setPassword(PW);
		   return textEncryptor.encrypt(text);
	   }
	   
	   public static String decrypt (String text){
		   textEncryptor.setPassword(PW);
		   return textEncryptor.decrypt(text);
	   }
	   /**
	    * Encodes a string using DEFAULT_ENCODING
	    * @param text : Text to encode
	    * @return encoded string
	    * 
	    * @deprecated use {@link encrypt()} instead.
	    */
	   @Deprecated
	   public static String base64encode(String text){
	      try {
	         String rez = enc.encode( text.getBytes( DEFAULT_ENCODING ) );
	         return rez;         
	      }
	      catch ( UnsupportedEncodingException e ) {
	         return null;
	      }
	   }//base64encode

	   /**
	    * Decodes a string using DEFAULT_ENCODING
	    * @param text : Text to decode
	    * @return decoded string
	    * 
	    * @deprecated use {@link decrypt()} instead.
	    */
	   @Deprecated
	   public static String base64decode(String text){

	         try {
	            return new String(dec.decodeBuffer( text ),DEFAULT_ENCODING);
	         }
	         catch ( IOException e ) {
	           return null;
	         }

	      }//base64decode

	}//class