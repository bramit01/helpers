package nl.bramit.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.Data;

@Data
public class Blat {
	private static Logger LOGGER = LoggerFactory.getLogger(Blat.class.getName());


	private String _Filename;
	private String _OptionFile_of;
	private List<String> _RecipientList_to = new ArrayList<>();
	private String _RecipientList_Filename_tf;
	private List<String> _CarbonCopyRecipientList_cc = new ArrayList<>();
	private String _CarbonCopyRecipientList_Filename_cf;
	private List<String> _BlindCarbonCopyRecipientList_bcc = new ArrayList<>();
	private String _BlindCarbonCopyRecipientList_Filename_bf;
	private String _maxNamesNumber_maxNames;
	private String _UndisclosedRecipients_ur;
	private String _Subject_s;
	private boolean _SuppressSubjectLine_ss;
	private String _Subject_Filename_sf;
	private String _Body;
	private String _Signature_Filename_sig;
	private String _Taglines_Filename_tag;
	private String _FinalMessageText_Filename_ps;
	
	//registry overrides
	private String _Profile_p;
	private String _SMPTServerName_server;
	private String _NNTPServerName_serverNNTP;
	private String _POP3ServerName_serverPOP3;
	private String _SenderAddress_f; //override default, address must be known to the server
	private String _FromAddress_i; //a 'From:' address not necassarily known to the server
	private String _SMTPPort_port = "25"; //defaults to 25
	private String _NNTPPort_portNNTP = "119"; //defaults to 119
	private String _POP3Port_portPOP3 = "110"; //defaults to 110
	private String _Username_u;
	private String _Password_pw;
	private String _POP3Username_pu;
	private String _POP3Password_ppw;
	
	//miscellaneous switches
	private String _Organization_org;
	private boolean _UserAgentHeaderLine_ua;
	private String _CustomXHeader_x;
	private boolean _NoHomepage_noh;
	private boolean _NoHeader_noh2;
	private boolean _DispositionNotification_d;
	private boolean _RequestReturnReceipt_r;
	private String _Charset_charset; //default is ISO-8859-1
	private String _CustomHeaderLine_a1;
	private String _CustomHeaderLine2_a2;
	private String _UseDeliveryStatusNOtifications_dsn; //n = never; s = succesfull; f = failure; d = delayed
	private boolean _UseBase64Headers_hdrencb;
	private boolean _UseQuotePrintableHeaders_hdrencq;
	private String _MsgPriority_priority; //0 = low; 1 = high;
	
	//attachement and encoding
	private List<String> _BinaryFileList_attach = new ArrayList<>();
	private String _BinaryFileList_Filename_af;
	private List<String> _TextFileList_attacht = new ArrayList<>();
	private String _TextFileList_Filename_atf;
	private List<String> _TextFileList_Inline_attachi = new ArrayList<>();
	private List<String> _FileToEmbedList_embed = new ArrayList<>();
	private String _FileToEmbedList_Filename_aef;
	private boolean _Base64;
	private boolean _Uuencode;
	private boolean _Enriched;
	private boolean _Unicode;
	private boolean _Html;
	private String _AltText;
	private String _AltText_Filename_alttextf;
	private boolean _Mime;
	private boolean _8BitMime;
	private String _Multipart; //size per 1000 bytes
	private boolean _NoMultipart_nomps;
	
	//NNTP options
	private List<String> _NewsGroupList_groups;
	
	//other options
	private boolean _Quiet_q = true;
	private boolean _Debug;
	private String _Logfile_log;
	private boolean _Timestamp;
	private String _Timeout_ti;
	private String _Try;
	private boolean _Binary;
	private String _Hostname;
	private boolean _Raw;
	private String _Delay;
	private String _Comment;
	private boolean _Superdebug;
	private boolean _Superdebugt;
	
	Blat(){
		
	}
	Blat(String server){
		set_SMPTServerName_server(server);
	}
	
	Blat(String server, String port){
		set_SMPTServerName_server(server);
		set_SMTPPort_port(port);
	}
		
	public ArrayList<String> getBlatSimpleMailParams(){
		ArrayList<String> paramList = new ArrayList<>();
		paramList.add("blat.exe");
		paramList.add("-server"); paramList.add(get_SMPTServerName_server());
		paramList.add("-port"); paramList.add(get_SMTPPort_port());
		paramList.add("-to"); paramList.add(StringUtils.join(get_RecipientList_to(), ", "));
		paramList.add("-f"); paramList.add(get_SenderAddress_f());
		paramList.add("-s"); paramList.add(get_Subject_s());
		paramList.add("-body"); paramList.add(get_Body());
		if (is_Debug()){
			paramList.add("-debug");
		}
		return paramList;
	}
	
	@SuppressWarnings("unchecked")
	public int sendSimpleMail(){
		LOGGER.debug("Blat parameterstring = [{}]", StringUtils.join(getBlatSimpleMailParams()));
		
		try{
			ProcessBuilder pb = new ProcessBuilder(getBlatSimpleMailParams());
			Process blatProcess = pb.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(blatProcess.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ( (line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			String result = builder.toString();
			LOGGER.debug(result);
	
			reader = new BufferedReader(new InputStreamReader(blatProcess.getErrorStream()));
			builder = new StringBuilder();
			line = null;
			while ( (line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			result = builder.toString();
			LOGGER.debug(result);
			
			return blatProcess.exitValue();
		
		}catch(Exception e){
			LOGGER.debug("Error on executing blat.exe", e);
			}
		return -9;
		}
	}
		

