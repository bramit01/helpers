package nl.bramit.helpers;


import nl.bramit.helpers.exceptions.LogManagerException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.joran.spi.JoranException;

/**
 * @author waardtj
 *
 */
public class LogManager {

	public static Logger getNewLogger(String p_sLogFile, String p_sClassName){
		String sLogFile = p_sLogFile;
		Logger LOGGER = LoggerFactory.getLogger(p_sClassName);

		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

		FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
		fileAppender.setContext(loggerContext);
		fileAppender.setName("timestamp");
		    // set the file name
		fileAppender.setFile(sLogFile);

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
		encoder.start();

		fileAppender.setEncoder(encoder);
		fileAppender.start();

		// attach the rolling file appender to the logger of your choice
		((ch.qos.logback.classic.Logger) LOGGER).addAppender(fileAppender);
		
		return LOGGER;
	}

	/**
	 * Creates LOGGER object from inputparameters
	 * 
	 * @param p_sLogbackXML
	 * @param p_sLogFileName
	 * @param p_sLogFilePath
	 * @param p_sClassName
	 * @return LOGGER
	 * @throws LogManagerException
	 */
	public static Logger getNewLoggerFromFile(
			String p_sLogbackXML
			, String p_sClassName
			, String p_sLogFilePath
			, String p_sLogFileName) throws LogManagerException  {

		if (StringUtils.isNotBlank(p_sLogbackXML) &&
				StringUtils.isNotBlank(p_sLogFilePath) &&
				StringUtils.isNotBlank(p_sLogFileName) &&
				StringUtils.isNotBlank(p_sClassName)) {

			String sLogBackXML = p_sLogbackXML;
			String sClassName = p_sClassName;
			String sLogFileName = p_sLogFileName;
			String sLogFilePath = p_sLogFilePath;
			
			Logger LOGGER = null;
			try {
				LOGGER = LoggerFactory.getLogger(sClassName);
				LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
				
				try {
					JoranConfigurator joranConfigurator = new JoranConfigurator();
					joranConfigurator.setContext(loggerContext);
					loggerContext.reset();
					//set logfilename and path
					loggerContext.putProperty("logFilePath", sLogFilePath);
					loggerContext.putProperty("logFileName", sLogFileName);
					joranConfigurator.doConfigure(sLogBackXML);
				}catch (JoranException je) {
					throw new LogManagerException("Error in getNewLoggerFromFile", je);
				}
				//StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);
			} catch (Exception ex) {
				throw new LogManagerException("Error in getNewLoggerFromFile", ex);
			}
			
			return LOGGER;
		}else {
			throw new LogManagerException("Error in calling getNewLoggerFromFile(), one of the inputparameters is null or empty");
		}
	}
	
}
