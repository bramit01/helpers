package nl.bramit.helpers;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.opencsv.CSVWriter;

public class StaticUtilsTest {

	@Test
	public void test1() {
		StaticUtils.xlsxToXlsx("\\\\grsfrs\\boxi_batch$\\storage\\temp\\20170817\\FLEXCUBE\\FLX_REG_NOS012 - Internal Deals Reconciliation_20170817_091639.xls");
		assert(true);
	}
	@Test
	public void test2() throws IOException {
		StaticUtils.txtToCSV("\\\\grsfrs\\boxi_batch$\\FLX_REG_OPS005 - Suspicious Transaction Monitoring.txt", "\\\\grsfrs\\boxi_batch$\\test.csv", CSVWriter.NO_QUOTE_CHARACTER, ';');
		assert(true);
	}

}
