package nl.bramit.helpers;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;

/**
 * Class with static util functions
 * @version 1.0.0
 * @author johandewaardt
 *
 */
public class StaticUtils {
	private static Logger LOGGER = LoggerFactory.getLogger(StaticUtils.class.getName());
	private static String _VERSION = StaticUtils.getVersion(StaticUtils.class.getResourceAsStream("/helpers.properties"));

	public static boolean sendEmail(
			String toAddress
			,String subject
			,String message){
		return sendEmail(
				"smtprelay"
				,"25"
				,"lon.busobj"
				,"Busine$$object2"
				,"fm.global.grssupport@rabobank.com"
				,toAddress
				,subject
				,message);
				
	}
	
	public static boolean sendEmail (
			String smtpServer
			,String smtpServerPort
			,String smtpAuthUser
			,String smtpAuthPw
			,String fromAddress
			,String toAddress
			,String subject
			,String message){
		Blat blat = new BlatBuilder()
				.with_SMPTServerName_server(smtpServer)
				.with_SMTPPort_port(smtpServerPort)
				.with_Username_u(smtpAuthUser)
				.with_SenderAddress_f(fromAddress)
				.withAdded_RecipientList_toElement(toAddress)
				.with_Subject_s(subject)
				.with_Body(message)
				.build();
		LOGGER.debug("returnvalue SendSimpleMail [{}]", blat.sendSimpleMail());
		return true;
	}
	/**
	 * http://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
	 * @param s
	 * @return
	 */
	public static boolean isInteger(String s) {
	    if (s != null){
	    	return isInteger(s,10);
	    	}else{
	    		return false;
	    	}
	}

	public static boolean isInteger(String s, int radix) {
	    if(s.isEmpty()) return false;
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            if(s.length() == 1) return false;
	            else continue;
	        }
	        if(Character.digit(s.charAt(i),radix) < 0) return false;
	    }
	    return true;
	}
	
	public static String get_VERSION() {
		return _VERSION;
	}

	/**
	 * 1.0.2 - JdW - 20150225 - TFS25674 
	 * 		changed txt2csv and xls2csv methods	
	 * @param p_sInputFile
	 * @param p_s_OutputFile
	 * @param p_charDelimiter
	 * @param p_charSeparator
	 * @throws IOException
	 */
	public static String getVersion(InputStream propertiesStream){
		//return version stored in pom.xml through a properties file
	
		Properties prop = new Properties();
		InputStream in = propertiesStream;
		try {
			prop.load(in);
		} catch (IOException e) {
			LOGGER.warn("Could not open properties file",e);
			return "N/A";
		} catch (NullPointerException e){
			LOGGER.warn("Properties file is not available", e);
			return "N/A";
		}finally{
			try {
				in.close();
			} catch (Exception e) {
				//do nothing
			}
		}
		
		return (String) prop.getOrDefault("program.version", "N/A");
	}
	
	public static void txtToCSV(String p_sInputFile, String p_s_OutputFile, char p_charDelimiter, char p_charSeparator) throws IOException{
		LOGGER.debug("Start conversion");
		CSVReader my_csv_input = null;
		CSVWriter my_csv_output = null;

		try {
			if (!createLockFile(p_s_OutputFile)) throw new IOException("Could not create lockfile for: " + p_s_OutputFile);
			//first line may contain a byte order mark (hex EF BB BF)
			//BOMInputStream takes care of this
			//BOMInputStream bom = new BOMInputStream(new FileInputStream(p_sInputFile), false);
			Path filePath = new File(p_sInputFile).toPath();
			//Charset charset = Charset.defaultCharset();  

			BOMInputStream bom = new BOMInputStream(Files.newInputStream(filePath), false);
			if (bom.hasBOM()){
				LOGGER.debug("BOM in file: [{}]", p_sInputFile);
			}
			//20160314: Added IgnoreQuotations and used builders instead of constructors
			final CSVParser readParser = new CSVParserBuilder() 
										.withSeparator('\t') 		//read tab-separated txt file
										.withIgnoreQuotations(true) //ignore quotes in fields
										.build(); 
			my_csv_input = new CSVReaderBuilder(new InputStreamReader(bom, "UTF-8")) 
										.withCSVParser(readParser) 
										.build();
			
			BufferedWriter out = Files.newBufferedWriter(Paths.get(p_s_OutputFile), StandardCharsets.UTF_8);
			
			my_csv_output = new CSVWriter(
					out
					,p_charSeparator
					,p_charDelimiter
					,CSVWriter.NO_ESCAPE_CHARACTER
					,System.lineSeparator()); 
			/* olde: my_csv_output = new CSVWriter(new FileWriter(p_s_OutputFile)
										,p_charSeparator
										,p_charDelimiter
										,CSVWriter.NO_ESCAPE_CHARACTER
										,System.lineSeparator()); end old*/ 
			String [] nextLineIn;

			while ((nextLineIn = my_csv_input.readNext()) != null) {
				List<String> nextLineOutList = new ArrayList<>();
				
				nextLineOutList = handleLine(nextLineIn, p_charSeparator, p_charDelimiter);
				
				my_csv_output.writeNext(nextLineOutList.toArray(new String[nextLineOutList.size()]));

			}//end while
			LOGGER.debug("[{}]; Lines read: [{}]",p_sInputFile, my_csv_input.getLinesRead());
			
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} finally{
			if (my_csv_input != null){
				my_csv_input.close();	
			}
			if (my_csv_output != null){
				my_csv_output.close();				
			}
			//this indicates that the conversion process has been ended
			removeLockFile(p_s_OutputFile);
		}

		
	}
	private static boolean createLockFile(String p_s_OutputFile) {
		//make sure extension is changed to .lock
		
		List<String> lines = Arrays.asList("Lock file for outputfile: " + p_s_OutputFile, new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		Path file = Paths.get(renameFileExtension(p_s_OutputFile, "lock"));


		try {
			if (Files.exists(file)) Files.delete(file);
			
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			LOGGER.error("Could not create or delete lockfile: [{}]", file, e);
			return false;
		} 
		
		if (Files.exists(file)) {
			return true;
		}else {
			return false;
		}
	}
	
	private static void removeLockFile(String p_s_OutputFile){
		Path file = Paths.get(renameFileExtension(p_s_OutputFile, "lock"));
		if (Files.exists(file))
			try {
				Files.delete(file);
			} catch (IOException e) {
				LOGGER.error("Could not delete existing lockfile: [{}]", file, e);
			}
	}

	private static List<String> handleLine(String[] nextLineIn, char separator, char delimiter) {
		List<String> out = new ArrayList<>();
	
		for(String line : nextLineIn){
			//20141017: Make sure to replace the charSeparator with empty string!
			//20150225: Only if there is no quote character defined
			//20160210: If line contains tab character (e.g. due to single doublequote char) split the line
			if (line.contains("\t")){
				//for some reason the line contains only \n for linebreaks, so add cr
				line = line.replace("\n", "\r\n");
				String[] lineArray = line.split("\t");
				for (String myString : lineArray){
					if (delimiter == CSVWriter.NO_QUOTE_CHARACTER){
						//LOGGER.debug("==> before replace [{}] : [{}]", Character.toString(separator), myString);
						out.add(myString.replace(Character.toString(separator), ""));
						//LOGGER.debug("==> after replace [{}] : [{}]", Character.toString(separator), myString);
					}else{
						out.add(myString);
					} //end if					
				}
			}else{
				if (delimiter == CSVWriter.NO_QUOTE_CHARACTER){
					//LOGGER.debug("==>2 before replace [{}] : [{}]", Character.toString(separator), line);
					out.add(line.replace(Character.toString(separator), ""));
					//LOGGER.debug("==>2 after replace [{}] : [{}]", Character.toString(separator), line);
				}else{
					out.add(line);
				} //end if				
			}
		}//end for
		return out;
	}

	/**
	 * Convert Excel file (97-2003) to CSV 
	 * @param p_sInputFile		.xls file, no checks
	 * @param s_OutputFile		.csv file, no checks
	 * @param p_charDelimiter	Column delimiter
	 * @param p_charSeparator	Column separator
	 * @throws IOException
	 * Libraries used: 
	 * 	opencsv : to write csv file
	 * 	apache poi: to read xls file
	 */
	public static void xlsToCSV (String p_sInputFile, String s_OutputFile, char p_charDelimiter, char p_charSeparator) throws IOException {
		
				//define formatter to convert fields to string
				//20150225:JdW: emulateCSV parameter added to DataFormatter
				DataFormatter format = new DataFormatter(true);
                //First we read the Excel file in binary format into FileInputStream
                FileInputStream input_document = null;
                CSVWriter my_csv_output = null;
                HSSFWorkbook my_xls_workbook = null;
				try {
					input_document = new FileInputStream(new File(p_sInputFile));
					// Read workbook into HSSFWorkbook
					// An exception can occur for .xls files created by BusinessObjects: Unexpected missing row when some rows are already present
					my_xls_workbook = new HSSFWorkbook(input_document); 
					// OpenCSV writer object to create CSV file
					FileWriter my_csv=new FileWriter(s_OutputFile);
					my_csv_output=new CSVWriter(
							my_csv
							,p_charSeparator
							,p_charDelimiter
							,CSVWriter.NO_ESCAPE_CHARACTER
							);

					for (int s=0; s<my_xls_workbook.getNumberOfSheets();s++) {
						// Read worksheet into HSSFSheet              
						HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(s); 
						// To iterate over the rows
						Iterator<Row> rowIterator = my_worksheet.iterator();	
						//Loop through rows.
						while(rowIterator.hasNext()) {
							String[] csvdata = null;
	                        Row row = rowIterator.next(); 
	                        int i=0;//String array
	                        //change this depending on the length of your sheet
	                        int ArraySize = row.getLastCellNum();
	                        //LOGGER.debug("ArraySize: {}",ArraySize);
	                        if (ArraySize > 0){
	                        	//JdW:PBI24828 -> 
	                            csvdata = new String[ArraySize];
	                            Iterator<Cell> cellIterator = row.cellIterator();
	                            while(cellIterator.hasNext()) {
	                            	Cell cell = cellIterator.next(); //Fetch CELL
	                                if (i <= (ArraySize - 1)){
	                                	//BUG25076:JdW: Replace column separator with empty char
	                                	//BUG25674:JdW: Only replace when delimiter = NONE
	                					if (p_charDelimiter == CSVWriter.NO_QUOTE_CHARACTER){
	                						csvdata[i]= format.formatCellValue(cell).replace(Character.toString(p_charSeparator), "");
	                					}else{
	                						csvdata[i]= format.formatCellValue(cell);
	                					}
	                                	//LOGGER.debug("i= {} : {}",i,csvdata[i]);
	                                }
	                                                                              
	                                i=i+1;
	                             }
	                        	
	                        }else{
	                        	//System.out.println("ArraySize !>0 " + ArraySize);
	                        }
	                        
	                        my_csv_output.writeNext(csvdata);
						}//end while
					
					}
					
					
				} finally{
	               if (my_csv_output != null){
	            	   my_csv_output.close(); //close the CSV file
	               }
	               input_document.close(); //close xls
	               if (my_xls_workbook != null) {
	            	   my_xls_workbook.close();
	               }
				}

        }

	/**
	 * Convert Excel file (2007+) to CSV 
	 * @param p_sInputFile		.xlsx file, no checks
	 * @param s_OutputFile		.csv file, no checks
	 * @param p_charDelimiter	Column delimiter
	 * @param p_charSeparator	Column separator
	 * @throws IOException
	 * Libraries used: 
	 * 	opencsv : to write csv file
	 * 	apache poi: to read xls file
	 * @throws InvalidFormatException 
	 */
	public static void xlsxToCSV_old (String p_sInputFile, String s_OutputFile, char p_charDelimiter, char p_charSeparator) throws IOException, InvalidFormatException {
				LOGGER.debug("==> start xlsxToCsv, parameters: [{}], [{}], [{}], [{}]"
						,p_sInputFile, s_OutputFile, p_charDelimiter, p_charSeparator );
				//define formatter to convert fields to string
				//20150225:JdW: emulateCSV parameter added to DataFormatter
				DataFormatter format = new DataFormatter(true);
                //First we read the Excel file in binary format into FileInputStream
                //FileInputStream input_document = null;
				File input_document = null;
				OPCPackage opcPackage = null;
                CSVWriter my_csv_output = null;
                XSSFWorkbook my_xls_workbook = null;
				try {
					input_document = new File(p_sInputFile);
					LOGGER.debug("==> input_document instantiated");
					// Read workbook into HSSFWorkbook
					// An exception can occur for .xls files created by BusinessObjects: Unexpected missing row when some rows are already present
					opcPackage = OPCPackage.open(input_document);
					LOGGER.debug("==> opcPackage instantiated");
					my_xls_workbook = new XSSFWorkbook(opcPackage);
					LOGGER.debug("==> my_xls_workbook instantiated");
					// OpenCSV writer object to create CSV file
					FileWriter my_csv=new FileWriter(s_OutputFile);
					my_csv_output=new CSVWriter(
							my_csv
							,p_charSeparator
							,p_charDelimiter
							,CSVWriter.NO_ESCAPE_CHARACTER
							);
					LOGGER.debug("==> my_csv_output instantiated");
					for (int s=0; s<my_xls_workbook.getNumberOfSheets();s++) {
						
						// Read worksheet into HSSFSheet              
						XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(s); 
						LOGGER.debug("==> my_worksheet instantiated");
						// To iterate over the rows
						Iterator<Row> rowIterator = my_worksheet.iterator();	
						//Loop through rows.
						while(rowIterator.hasNext()) {
							String[] csvdata = null;
	                        Row row = rowIterator.next();
	                        LOGGER.debug("==> Row: [{}]",row.getRowNum());
	                        int i=0;//String array
	                        //change this depending on the length of your sheet
	                        int ArraySize = row.getLastCellNum();
	                        //LOGGER.debug("ArraySize: {}",ArraySize);
	                        if (ArraySize > 0){
	                        	//JdW:PBI24828 -> 
	                            csvdata = new String[ArraySize];
	                            Iterator<Cell> cellIterator = row.cellIterator();
	                            while(cellIterator.hasNext()) {
	                            	Cell cell = cellIterator.next(); //Fetch CELL
	                                if (i <= (ArraySize - 1)){
	                                	//BUG25076:JdW: Replace column separator with empty char
	                                	//BUG25674:JdW: Only replace when delimiter = NONE
	                					if (p_charDelimiter == CSVWriter.NO_QUOTE_CHARACTER){
	                						csvdata[i]= format.formatCellValue(cell).replace(Character.toString(p_charSeparator), "");
	                					}else{
	                						csvdata[i]= format.formatCellValue(cell);
	                					}
	                                	//LOGGER.debug("i= {} : {}",i,csvdata[i]);
	                                }
	                                                                              
	                                i=i+1;
	                             }
	                        	
	                        }else{
	                        	//System.out.println("ArraySize !>0 " + ArraySize);
	                        }
	                        
	                        my_csv_output.writeNext(csvdata);
						}//end while
					
					}
					
					
				} finally{
	               if (my_csv_output != null){
	            	   my_csv_output.close(); //close the CSV file
	               }
	               input_document = null; //close xls
	               if (my_xls_workbook != null) {
	            	   my_xls_workbook.close();
	               }
				}

        }
	/**
	 * Open Excel file, save it with a temporary name and move file back
	 * **workaround for a BusinessObjects bug: KBA 2186523
	 * @param xlsxFileIn
	 */
	public static void xlsxToXlsx (String xlsxFileIn){
		FileOutputStream out = null;
		XSSFWorkbook wb = null;
		try {
            //open file
            wb = new XSSFWorkbook(new FileInputStream(new File(xlsxFileIn)));
            //remove empty last rows
           // for (int i = 0; i < wb.getNumberOfSheets(); i++){
           // 	XSSFSheet sheet = wb.getSheetAt(i);
           // 	XlsxRemoveEmptyRows(sheet);
           // }
            //save file
            out = new FileOutputStream(xlsxFileIn + ".tmp");
            if ((wb != null)&&(out != null)){
            	wb.write(out);
            }
        } catch (IOException ex) {
        	LOGGER.warn("==> xlsxToXlsx(): Error in reading [{}], or writing [{}]", xlsxFileIn, xlsxFileIn + ".tmp", ex);
        }finally{
        	try {
        		if (out != null) out.close();
				if (wb != null) wb.close();
			} catch (IOException e) {
				//do nothing
			}
            MoveFile(xlsxFileIn + ".tmp", xlsxFileIn);
        }
		
	}
	private static void XlsxRemoveEmptyRows(XSSFSheet hsheet){
		boolean stop = false;
        boolean nonBlankRowFound;
        short c;
        XSSFRow lastRow = null;
        XSSFCell cell = null;

        while (stop == false) {
            nonBlankRowFound = false;
            lastRow = hsheet.getRow(hsheet.getLastRowNum());
            for (c = lastRow.getFirstCellNum(); c <= lastRow.getLastCellNum(); c++) {
                cell = lastRow.getCell(c);
                if (cell != null && lastRow.getCell(c).getCellType() != HSSFCell.CELL_TYPE_BLANK) {
                    nonBlankRowFound = true;
                }
            }
            if (nonBlankRowFound == true) {
                stop = true;
            } else {
                hsheet.removeRow(lastRow);
            }
        }

        
	}
	/**
	 * Convert Excel file (2007+) to CSV 
	 * @param p_sInputFile		.xlsx file, no checks
	 * @param s_OutputFile		.csv file, no checks
	 * @param p_charDelimiter	Column delimiter
	 * @param p_charSeparator	Column separator
	 * @throws IOException
	 * Libraries used: 
	 * 	opencsv : to write csv file
	 * 	apache poi: to read xls file
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws OpenXML4JException 
	 */
	public static void xlsxToCSV (String xlsxFile, String csvFile, char txtDelimiter, char colSeparator) throws IOException, OpenXML4JException, ParserConfigurationException, SAXException {
				LOGGER.debug("==> start xlsxToCsv, parameters: [{}], [{}], [{}], [{}]"
						,xlsxFile, csvFile, txtDelimiter, colSeparator );
		        OPCPackage p = OPCPackage.open(new File(xlsxFile).getPath(), PackageAccess.READ);
				XLSX2CSV xlsx2csv = new XLSX2CSV(p, new File(csvFile), txtDelimiter, colSeparator);
				xlsx2csv.process();
				p.close();

        }

	  public static String renameFileExtension
	  (String source, String newExtension)
	  {
	    String target;
	    String currentExtension = getFileExtension(source);

	    if (currentExtension.equals("")){
	      target = source + "." + newExtension;
	    }
	    else {
	      target = source.replaceFirst(Pattern.quote("." +
	          currentExtension) + "$", Matcher.quoteReplacement("." + newExtension));

	    }
	    return target;
	  }

	  public static String getFileExtension(String f) {
	    String ext = "";
	    int i = f.lastIndexOf('.');
	    if (i > 0 &&  i < f.length() - 1) {
	      ext = f.substring(i + 1);
	    }
	    return ext;
	  }
	public static String getPathFrom(String p_sPathAndFile){
		//return path (also if input is already a path)
		String pathAndFile = p_sPathAndFile;
		File file = new File(pathAndFile);
		String path = file.getParent();
		if (path.equals("")){
			return pathAndFile;
		}else{
			return path;
		}
	}
	
	
	/**
	 * 
	 * @param t
	 * @return
	 */
	public static String getStackTrace(Throwable t) {
		
		  {
		        StringWriter sw = new StringWriter();
		        PrintWriter pw = new PrintWriter(sw, true);
		        t.printStackTrace(pw);
		        pw.flush();
		        sw.flush();
		        return sw.toString();
		    }

	}
	
	/**
	 * return path of the passed class
	 * @param p_oClass
	 * @return
	 */
	public static String getCleanPath(Class<?> p_oClass){
		
		URL location = p_oClass.getProtectionDomain().getCodeSource().getLocation();
        return getPathFrom(location.getFile());
        
	}
	/**
	 * 
	 * @param p_oClass
	 * @return
	 */
	public static String getPathAndNameOf(Class<?> p_oClass){
		
		URL location = p_oClass.getProtectionDomain().getCodeSource().getLocation();
        return (location.getFile()+p_oClass.getSimpleName());
        
	}
	
	/**
	 * Adds slash to the end of the string if it is not already there
	 * 
	 * @param p_sPath
	 * @return
	 */
	public static String addTrailingSlash(String p_sPath){
		String sPath = p_sPath;
		if (sPath != null){
			if (sPath.length() > 1){
				if(sPath.charAt(sPath.length()-1)!=File.separatorChar){
					sPath += File.separator;
				}
			}	
		}
		
		return sPath;
	}
	/**
	 * Converting the arraylist to a hashset de-doubles the list
	 * 
	 * @param lstTriggerPaths
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<String> removeDoubles(List<String> lstTriggerPaths){
		ArrayList al = new ArrayList();
		al = (ArrayList) lstTriggerPaths;
		HashSet hs = new HashSet();
		hs.addAll(al);
		al.clear();
		al.addAll(hs);
		return al;
	}
	
	public static Boolean checkFolderAndCreate(String p_sFolderPath) {
		Boolean bResult = false;
		if (StringUtils.isNotBlank(p_sFolderPath)) {
			String sFolderPath = p_sFolderPath;
			Path path = Paths.get(sFolderPath);
			if (!Files.exists(path)) {
				//create one
				Boolean success = (new File(sFolderPath)).mkdirs();
				if (!success) {
					bResult = false;
				}else {
					bResult = true;
				}
			}else {
				bResult = true;
			}
		}
		return bResult;
	}
	//temporary class for System json representation
	static class Temp{
		String System;
		@Override
		public String toString(){
			return this.System;
		}
	}

	public static String getGsonStringFrom(String description, String member) {
		String returnString = "";
		
		String json = getStringBetweenBrackets(description);
		if (!StringUtils.isBlank(json)){
			Gson gson = new GsonBuilder().create();
			try{
				Temp temp = gson.fromJson(json, Temp.class);
				if (temp != null){returnString = temp.System;}
			}catch (Exception e){
				LOGGER.warn("Exception in getGsonStringFrom, parameters [{}], [{}]", description, member,e);			
			}
			LOGGER.debug("getGsonStringFrom: description [{}], json [{}], returnString [{}]", description, json, returnString);
		}
		
		return returnString;
	}
	
	public static String getStringBetweenBrackets(String input){
		String output = "";
		Pattern pattern = Pattern.compile("\\[(.*?)\\]");
		Matcher matcher = pattern.matcher(input);
		if (matcher.find()){
			LOGGER.debug("getStringBetweenBrackets: Match found on input [{}]", input);
			output = matcher.group(1);
		}
		return output;
	}
	/**
	 * checks file if it is Binary (XLSX or XLS) or TXT
	 * @param filename
	 * @return
	 * returns NULL on exception
	 */
	public static String checkFile(String filename){
		try {
			if (isBinaryFile(new File(filename))){
				try (FileInputStream fis = new FileInputStream(filename)){
					BufferedInputStream bis = new BufferedInputStream(fis);
					boolean isXlsx = POIXMLDocument.hasOOXMLHeader(bis);
					bis.reset();
					boolean isXls = POIFSFileSystem.hasPOIFSHeader(bis);

					if (isXlsx){
						return("XLSX");
					}else if (isXls){
						return("XLS");
					}else{
						return("UNKNOWN");
					}
				}catch(Exception e){
					LOGGER.warn("Exception occured in checking file: [{}]", filename, e);
					return "";
				}
			}else{
				return("TXT");
			}
		} catch (FileNotFoundException e){
			LOGGER.warn("FileNotFoundException occured: [{}]", filename, e);
			return "NOTFOUND";
		} catch (Exception e) {
			LOGGER.warn("Exception occured in checking file: [{}]", filename, e);
			return "";
		}
		
	}
	/**
	 * search case independent through an enum class
	 * @param enumeration
	 * @param search
	 * @return
	 */
	public static <T extends Enum<?>> T searchEnum(Class<T> enumeration, String search){
		for (T each : enumeration.getEnumConstants()){
			if (each.name().compareToIgnoreCase(search) == 0){
				return each;
			}
		}
		return null;
	}
	
	/**
	 *  Guess whether given file is binary. Just checks for anything under 0x09.
	 */
	private static boolean isBinaryFile(File f) throws FileNotFoundException, IOException {
	    FileInputStream in = new FileInputStream(f);
	    int size = in.available();
	    if(size > 1024) size = 1024;
	    byte[] data = new byte[size];
	    in.read(data);
	    in.close();

	    int ascii = 0;
	    int other = 0;

	    for(int i = 0; i < data.length; i++) {
	        byte b = data[i];
	        if( b < 0x09 ) return true;

	        if( b == 0x09 || b == 0x0A || b == 0x0C || b == 0x0D ) ascii++;
	        else if( b >= 0x20  &&  b <= 0x7E ) ascii++;
	        else other++;
	    }

	    if( other == 0 ) return false;

	    return 100 * other / (ascii + other) > 95;
	}

	public static void MoveFile (String input, String output){
		Path FROM = Paths.get(input);
	    Path TO = Paths.get(output);
	    //overwrite existing file, if exists
	    CopyOption[] options = new CopyOption[]{
	      StandardCopyOption.REPLACE_EXISTING
	    }; 
	    try {
			Files.move(FROM, TO, options);
		} catch (IOException e) {
			LOGGER.warn("==> MoveFile(): Not able to move file [{}} to [{}]", input, output, e);
		}
	  }
}
