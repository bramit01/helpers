package nl.bramit.helpers;

import java.time.Clock;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import net.objectlab.kit.datecalc.common.DateCalculator;
import net.objectlab.kit.datecalc.common.DefaultHolidayCalendar;
import net.objectlab.kit.datecalc.common.HolidayCalendar;
import net.objectlab.kit.datecalc.common.KitCalculatorsFactory;
import net.objectlab.kit.datecalc.jdk8.LocalDateKitCalculatorsFactory;

public class TimeUtils {
	private static Logger LOGGER = LoggerFactory.getLogger(StaticUtils.class.getName());
	@ Getter private static Clock clock = Clock.systemDefaultZone();
	private static ZoneId zoneId = ZoneId.systemDefault();

	
	static KitCalculatorsFactory<LocalDate> kcf;
	static DateCalculator<LocalDate> calf;
	static DateCalculator<LocalDate> calb;

	public enum Pointer {
		CURRENT, NEXT, PREVIOUS
	}
	
	public enum Period {
		DAY, BUSINESSDAY, WEEK, MONTH, QUARTER, YEAR
	}
	
	public static LocalDateTime now(){
		return LocalDateTime.now(getClock());
	}
	
	public static void useFixedClockAt(LocalDateTime date){
		clock = Clock.fixed(date.atZone(zoneId).toInstant(), zoneId);
	}
	
	public static void useSystemDefaultZoneClock(){
		clock = Clock.systemDefaultZone();
	}
	
	/**
	 * add holiday calendar for a specific country
	 * @param country
	 * @param holidays
	 */
	public static void addHolidayCalendar(String country, Set<LocalDate> holidays){
		HolidayCalendar<LocalDate> calendar = new DefaultHolidayCalendar<LocalDate> (holidays);
		kcf = LocalDateKitCalculatorsFactory.getDefaultInstance().registerHolidays(country, calendar);
	}
	/**
	 * extract country from BUSINESSDAY string, see supported formats in convertDateTimeVariable(variable, country)
	 * @param variable
	 * @return
	 */
	public static String convertDateTimeVariable(String variable){
		String[] variableArray = variable.split("\\|");
		int index = 0;
		if (variableArray.length == 3){
			index = 1;
		}else if (variableArray.length == 4){
			index = 2;
		}
		String country = variableArray[index].replaceAll("BUSINESSDAY", "");
		//remove country from string (to be able to use enum Period.class
		variable = variable.replaceAll("BUSINESSDAY" + country , "BUSINESSDAY");
		return convertDateTimeVariable(variable, country);
	}
	/**
	 * convert string that contains DateTime variable
	 * the variable has the format: {pointer}[|{step}]|{period}|{format}
	 *  {pointer} CURRENT; NEXT; PREVIOUS
	 *  {step} numeric, steps in time forward or backward, is optional
	 *  {period} - DAY, BUISNESSDAY, WEEK, MONTH, QUARTER, YEAR
	 *  {format} valid format according to: https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
	 *  ==> for pointer=NEXT and pointer=PREVIOUS; period can be DAY, WEEK, MONTH, QUARTER, YEAR
	 *  ==> for pointer=CURRENT; period can be DAY or BUSINESSDAY; step is optional
	 *  ==> for period = BUSINESSDAY, only day/month/year possible
	 * 
	 * examples:
	 * 	CURRENT|DAY|yyyy MMM dd HH:mm:ss -> 2016 Oct 20 13:12:11
	 *  NEXT|2|BUSINESSDAYNL_yyyyMMdd	 -> 20161022 ; Second next business day according to NL holiday table
	 *  PREVIOUS|10|DAY|yyyyMMdd			 -> 20161011
	 *  PREVIOUS|2|WEEK|dd MM yyyy w	 -> 06 10 2016 23; 23 = week number of year
	 *  yyyy MMM dd HH:mm:ss 			 -> 2016 Oct 20 13:12:11
	 *  HHmmss							 -> 131211
	 *  yyyyMMdd.A						 -> 20161020.123456 (A = milli of day)
	 * @param variable
	 * @return
	 */
	public static String convertDateTimeVariable(String variable, String country){
		String[] variableArray = variable.split("\\|");
		int step = 0; //number of periods to be added to the current date
		String pointer = ""; // NEXT/CURRENT/PREVIOUS
		String period = "";
		String format = ""; //needs to be valid format (time JDK8)
		
		if (isReplacementFormatOK(variable)){
			if (variableArray.length == 1){
				//only date and or time - act as CURRENT_DAY 
				pointer = "CURRENT";
				step = 0;
				period = "DAY";
				format = variable;
			}else if (variableArray.length == 3){
				pointer = variableArray[0];
				step = 0; 
				period = variableArray[1];
				format = variableArray[2];
			}else if (variableArray.length == 4){
				pointer = variableArray[0];
				step = Integer.valueOf(variableArray[1]);
				period = variableArray[2];
				format = variableArray[3];
			}
		}else{
			//return complete inputstring unaltered 
			return variable;
		}
		return formatDateTimeAs(pointer, step, period, format, country);
	}

	/**
	 * return formatted datetime string based on the input and on the holiday calendar of the specified country
	 * @param pointer
	 * @param step
	 * @param period
	 * @param format
	 * @param country
	 * @return
	 */
	private static String formatDateTimeAs(String pointer, int step, String period, String format, String country) {
		LocalDateTime currentDate = now(); 
		LocalDate currentBusinessDay = null;
		String formattedDate = "";
		if (StaticUtils.searchEnum(Period.class, period) == Period.BUSINESSDAY){
			if ((kcf == null)||(!kcf.isHolidayCalendarRegistered(country))){
				LOGGER.warn("The country [{}] is not registered for a holidaycalendar, use the addHolidayCalendar() function", country);
				return "";
			}else{
				calf = LocalDateKitCalculatorsFactory.forwardCalculator(country);
				calb = LocalDateKitCalculatorsFactory.backwardCalculator(country);
				calb.setStartDate(currentDate.toLocalDate());
				currentBusinessDay = calb.getCurrentBusinessDate();
			}
		}
		
		try{
			if (StaticUtils.searchEnum(Pointer.class, pointer) == Pointer.NEXT){
				//we go forward
				if (step == 0) {step = 1;} //NEXT without step is always 1 forward
				if (StaticUtils.searchEnum(Period.class, period) == Period.DAY){
					formattedDate = currentDate.plusDays(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.WEEK){
					formattedDate = currentDate.plusWeeks(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.MONTH){
					formattedDate = currentDate.plusMonths(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.QUARTER){
					formattedDate = currentDate.plusMonths(step * 3).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.YEAR){
					formattedDate = currentDate.plusYears(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.BUSINESSDAY){
					formattedDate = calf.moveByBusinessDays(step).getCurrentBusinessDate().format(DateTimeFormatter.ofPattern(format));
				}
			}else if (StaticUtils.searchEnum(Pointer.class, pointer) == Pointer.PREVIOUS){
				//we go backwards
				if (step == 0) {step = 1;} //NEXT without step is always 1 backward
				if (StaticUtils.searchEnum(Period.class, period) == Period.DAY){
					formattedDate = currentDate.minusDays(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.WEEK){
					formattedDate = currentDate.minusWeeks(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.MONTH){
					formattedDate = currentDate.minusMonths(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.QUARTER){
					formattedDate = currentDate.minusMonths(step * 3).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.YEAR){
					formattedDate = currentDate.minusYears(step).format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.BUSINESSDAY){
					formattedDate = calb.moveByBusinessDays(step * -1).getCurrentBusinessDate().format(DateTimeFormatter.ofPattern(format));
				}
			}else if (StaticUtils.searchEnum(Pointer.class, pointer) == Pointer.CURRENT){
				//current - makes only sense for DAY and BUSINESSDAY
				if (StaticUtils.searchEnum(Period.class, period) == Period.DAY){
					LOGGER.debug("==> currentDate = [{}]; format = [{}]", currentDate, format);
					formattedDate = currentDate.format(DateTimeFormatter.ofPattern(format));
				}else if (StaticUtils.searchEnum(Period.class, period) == Period.BUSINESSDAY){
					formattedDate = currentBusinessDay.format(DateTimeFormatter.ofPattern(format));
				}else{
					LOGGER.error("Error in format of DateTime string: [{}], [{}] is not valid for [{}]", pointer + "|" + step + "|" + period + "|" + format, period, pointer);
				}
			}else{
				formattedDate = "**ERROR**";
			}
		}catch(DateTimeException e){
			LOGGER.error("Error in formatting [{}]", pointer + "|" + step + "|" + period + "|" + format, e );
		}
		
		return formattedDate;
	
	}
	/**
	 * check format of the passed variable
	 * @param variable
	 * @return true if ok, false if not; details in LOG
	 */
	public static boolean isReplacementFormatOK (String variable){
		boolean rt = false;
		String[] varArr = variable.split("\\|");
		int len = varArr.length;
		String format = "";
		
		switch (len) {
		case 1:
			//only time and/or date format
			format = variable;
			rt = true;
			break;
		case 3:
			//no step, so should have pointer, period, format
			if (StaticUtils.searchEnum(Pointer.class, varArr[0]) != null){
				if (StaticUtils.searchEnum(Period.class, varArr[1]) != null){
					format = varArr[2];
					rt = true;
				}else{
					LOGGER.error("Period contains an invalid value: [{}], in variable: [{}]", varArr[1], variable);
					rt = false;
				}
			}else{
				LOGGER.error("Pointer contains an invalid value: [{}], in variable: [{}]", varArr[0], variable);
				rt = false;
			}
			break;
		case 4:
			//including step, so should have pointer, step, period, format
			if (StaticUtils.searchEnum(Pointer.class, varArr[0]) != null){
				if (StaticUtils.searchEnum(Period.class, varArr[2]) != null){
					if (StringUtils.isNumeric(varArr[1])){
						format = varArr[3];
						rt = true;
					}else{
						LOGGER.error("Step contains an invalid value: [{}], in variable: [{}]", varArr[1], variable);
						rt = false;
					}
				}else{
					LOGGER.error("Period contains an invalid value: [{}], in variable: [{}]", varArr[2], variable);
					rt = false;
				}
			}else{
				LOGGER.error("Pointer contains an invalid value: [{}], in variable: [{}]", varArr[0], variable);
				rt = false;
			}
			
			break;
		default:
			LOGGER.error("Variable contains invalid number of parameters: [{}], variable: [{}]", len, variable);
			rt = false;
		}
		
		//check format by using fixed clock only if above checks are ok
		if (rt){

			try{
				LocalDateTime nu = now();
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
				String formatted = nu.format(formatter);
				if (formatted.equals("")){
					LOGGER.error("Format contains an invalid value: [{}], in variable: [{}]", format, variable);
					rt = false;	
				}			
			}catch(Exception e){
				LOGGER.error("Exception occured; Format contains an invalid value: [{}], in variable: [{}]", format, variable, e);
				rt = false;
			}
		}		
		return rt;
	}
}
